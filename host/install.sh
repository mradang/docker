#!/bin/bash

hostname=""

while [ ! $hostname ]; do
  read -p "请输入主机域名：" hostname
done

sudo \cp $bashpath/host/ssl.hostname.config /etc/nginx/conf.d/ssl.$hostname.config
sudo \cp $bashpath/host/status.hostname.sample /etc/nginx/conf.d/status.$hostname.conf
sudo sed -i "s/{hostname}/$hostname/" /etc/nginx/conf.d/ssl.$hostname.config
sudo sed -i "s/{hostname}/$hostname/" /etc/nginx/conf.d/status.$hostname.conf

sudo mkdir -p /etc/nginx/ssl
sudo chown $SUDO_USER:$SUDO_USER /etc/nginx/ssl/

userhome=$(getent passwd $SUDO_USER | cut -d: -f6)

sudo -u $SUDO_USER $userhome/.acme.sh/acme.sh --issue --dns dns_dp -d "*.$hostname"

sudo -u $SUDO_USER $userhome/.acme.sh/acme.sh --install-cert -d "*.$hostname" --key-file /etc/nginx/ssl/$hostname.key --fullchain-file /etc/nginx/ssl/$hostname.crt --reloadcmd "sudo systemctl force-reload nginx"
