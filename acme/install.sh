#!/bin/bash

sudo -u $SUDO_USER git clone https://gitee.com/neilpang/acme.sh.git
cd acme.sh
sudo -u $SUDO_USER ./acme.sh --install

userhome=$(getent passwd $SUDO_USER | cut -d: -f6)

sudo -u $SUDO_USER $userhome/.acme.sh/acme.sh --set-default-ca --server letsencrypt

DP_Id=""
while [ ! $DP_Id ]
do
  read -p "请输入 DP_Id：" DP_Id
done

DP_Key=""
while [ ! $DP_Key ]
do
  read -p "请输入 DP_Key：" DP_Key
done

sudo -u $SUDO_USER echo "SAVED_DP_Id='$DP_Id'" >> $userhome/.acme.sh/account.conf
sudo -u $SUDO_USER echo "SAVED_DP_Key='$DP_Key'" >> $userhome/.acme.sh/account.conf
