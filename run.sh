#!/bin/bash

export BOLD="$(tput bold)"
export YELLOW="$(tput setaf 3)"
export GREEN="$(tput setaf 2)"
export NC="$(tput sgr0)"

# 需要 sudo 执行
if [ "$EUID" -ne 0 ]; then
    echo "${YELLOW}需要 sudo 执行${NC}"
    exit 9
fi

# 仅支持 ubuntu 系统
os_info=$(lsb_release -a 2>/dev/null)
if [[ $os_info != *Ubuntu* ]]; then
    echo "${YELLOW}仅支持 ubuntu 系统${NC}"
    exit 9
fi

# 功能菜单
flag=true
while $flag; do
    echo "Ubuntu Docker 环境部署脚本：
1. 系统初始化
2. 安装 Docker
3. 安装 Nginx
4. 安装 acme.sh
5. 安装 Host"
    read -p "请选择任务：" channel
    expr $channel + 0 &>/dev/null
    [ $? -eq 0 ] && [ $channel -ge 1 ] && [ $channel -le 5 ] && flag=false
done

export bashpath=$(dirname $(readlink -f $0))

case $channel in
1)
    bash $bashpath/linux/install.sh
    ;;
2)
    bash $bashpath/docker/install.sh
    ;;
3)
    bash $bashpath/nginx/install.sh
    ;;
4)
    bash $bashpath/acme/install.sh
    ;;
5)
    bash $bashpath/host/install.sh
    ;;
esac

echo -e "\n"
exec $BASH_SOURCE
