#!/bin/bash

# 使用说明：files.backup.sh backupname days file1 dir1 file2 ...

# 备份名
dir=$1

# 保留天数
days=$2

# 备份文件列表
shift 2

# 指定备份目录
backupDir=$HOME/backup/$dir
[ -d $backupDir ] || mkdir -p $backupDir

# 备份
/usr/bin/tar Pczvf $backupDir/$dir\_$(date +\%Y\%m\%d).tar.gz $@

# 清理旧的备份文件
days=$[$days+1]
ls -t $backupDir/$dir\_*.tar.gz |tail -n +$days |xargs -r rm -f
